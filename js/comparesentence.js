function compareSentences(string1, string2) {
  var s = string1;
  var t = string2;
  var sWords = [];
  var tWords = [];
  var m = 0;
  var f = 0;

  // create array with lowercase words
  // sorted alphabetically
  // remove punctuation

  s = s.toLowerCase();
  t = t.toLowerCase();

  s = removePunctuation(s);
  t = removePunctuation(t);

  sWords = s.split(" ");
  sWords.sort();

  tWords = t.split(" ");
  tWords.sort();

  // remove the common words: a, an, the
  for (var i = 0; i < sWords.length; i++) {
    if (sWords[i] == "a" || sWords[i] == "an" || sWords[i] == "the") {
      sWords.splice(i, 1);
    }
  }

  for (var j = 0; j < tWords.length; j++) {
    if (tWords[i] == "a" || tWords[i] == "an" || tWords[i] == "the") {
      tWords.splice(i, 1);
    }
  }

  sWords = removeDuplicates(sWords);
  tWords = removeDuplicates(tWords);

  var p0 = 0;
  var p1 = 0;
  var comp;

  // compare words lexicographically
  do {
    comp = compareTo(sWords[p0], tWords[p1]);
    if (comp == 0) {
      // same word
      m++;
      p0++;
      p1++;
    } else if (comp > 0) {
      // s word comes before t word
      p0++;
    } else if (comp < 0) {
      // s word comes after t word
      p1++;
    }
  } while (p0 < sWords.length && p1 < tWords.length);

  f = (2.0 * m) / (sWords.length + tWords.length);
  f = f.toPrecision(5);
  //console.log("Found " + m + " matching words.");
  //console.log("Similarity factor: " + f);
  return f;

}

function compareTo(string1, string2) {
  if (string1 == string2) {
    return 0;
  } else if (string1 < string2) {
    return 1;
  } else if (string1 > string2) {
    return -1;
  }
}

function removeDuplicates(array) {
  var done = [];
  var newArray = [];

  for (var i = 0; i < array.length; i++) {
    if (done.indexOf(array[i]) == -1) {
      // once added to new array, it is done
      // if done it means it goes over a duplicate
      // duplicates are discarded
      newArray.push(array[i]);
      done.push(array[i]);
    }
  }

  return newArray;
}

function removePunctuation(string) {
  var punctRE = /[\u2000-\u206F\u2E00-\u2E7F\\'!"#$%&()*+,\-.\/:;<=>?@\[\]^_`{|}~]/g;
  var spaceRE = /\s+/g;
  return string.replace(punctRE, '').replace(spaceRE, ' ');
}