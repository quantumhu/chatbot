var api_key = "youtube";
var yt_ready = false;
var weather_api = "weather";
var player = document.getElementById("player").contentWindow;

var timer;

var timerSound;
var inputAcceptSound;

var reminders = [];

gapi.load("client", handleAPILoaded);

function handleAPILoaded() {
  gapi.client.setApiKey(api_key);
  gapi.client.load("youtube", "v3", function() {
  	//console.log("Youtube v3 API Loaded");
  	yt_ready = true;
  });
}

function playSong(id) {

  document.getElementById("player").src = "https://www.youtube.com/embed/" + id + "?autoplay=1&enablejsapi=1";
  console.log("Started playback");

}

function findSong(query, callback) {

  // prep request
  var request = gapi.client.youtube.search.list({
  	q: query,
  	part: "snippet",
  	maxResults: 1,
  	type: "video"
  });

  request.execute(function(response) {
  	if (response.items.length == 0) {
  	  callback("blah", "");
  	} else {
  	  callback(response.items[0].id.videoId, response.items[0].snippet.title);
  	}
  });

}

function pauseSong() {

  player.postMessage('{"event":"command","func":"pauseVideo","args":""}', '*');

}

function rePlaySong() {

  player.postMessage('{"event":"command","func":"playVideo","args":""}', '*');

}

function getWeatherCurrent(cityid, callback) {

  var xhrp;
  // compatible with IE7+, Firefox, Chrome, Opera, Safari
  xhrp = new XMLHttpRequest();
  xhrp.onreadystatechange = function(){
    if (xhrp.readyState == 4 && xhrp.status == 200){
      callback(xhrp.responseText);
    }
  }
  xhrp.open("GET", "https://cors-anywhere.herokuapp.com/http://api.openweathermap.org/data/2.5/weather?id=" + cityid + "&APPID=" + weather_api + "&units=metric", true);
  xhrp.send();

}

function getWeatherDay(cityid, callback) {

  var xhrp;
  // compatible with IE7+, Firefox, Chrome, Opera, Safari
  xhrp = new XMLHttpRequest();
  xhrp.onreadystatechange = function(){
    if (xhrp.readyState == 4 && xhrp.status == 200){
      callback(xhrp.responseText);
    }
  }
  xhrp.open("GET", "https://cors-anywhere.herokuapp.com/http://api.openweathermap.org/data/2.5/forecast?id=" + cityid + "&APPID=" + weather_api + "&units=metric", true);
  xhrp.send();

}

function ringTimer() {
  timerSound = new Audio("cesium.ogg");
  timerSound.addEventListener("ended", function() {
    this.currentTime = 0;
    this.play();
  }, false);
  timerSound.play();
  timerSound = null;
}

function stopTimer() {
  if (timerSound != null) {
    timerSound.pause();
    timerSound.currentTime = 0;
  }
}

function ringUnderstand() {
  inputAcceptSound = new Audio("comprehend.ogg");
  inputAcceptSound.play();
}

function getWorkShifts(txtfile, callback) {
  var xhrp;
  if (window.XMLHttpRequest) {
    xhrp = new XMLHttpRequest();
  } else {
    // code for older browsers
    xhrp = new ActiveXObject("Microsoft.XMLHTTP");
  }
  xhrp.onreadystatechange = function() {
    if (xhrp.readyState == 4 && xhrp.status == 200) {
      // split at each new line
      var tempArray = xhrp.responseText.split("\n");

      for (var i = 0; i < tempArray.length; i++) {
        tempArray[i] = tempArray[i].replace(/[\n\r]/g, "");
        tempArray[i] = tempArray[i].split("|");
      }

      callback(tempArray);
    }
  }
  xhrp.open("GET", txtfile, true);
  xhrp.send(null);
}

function addReminder(reminder) {
  reminders.push(reminder);
}

function remindYou() {
  if (reminders.length == 0) {
    pasteMessage("You have no reminders.", true);
  } else {
    var temp = "Reminders, ";
    for (var i = 0; i < reminders.length; i++) {
      temp = temp + (i + 1) + ": " + reminders[i] + ". ";
    }
    pasteMessage(temp, true);
  }
}