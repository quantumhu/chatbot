// SPEECH RECOGNITION
if (annyang) {
  var commands = {
  	"iris": function() {
  	  ringUnderstand();
  	  pasteMessage("Yes?", true);
  	},
  	"hello": function() {
  	  ringUnderstand();
  	  pasteMessage("Hello!", true);
  	},
  	"hi": function() {
  	  ringUnderstand();
  	  pasteMessage("Hi!", true);
  	},
  	"good morning": function() {
  	  ringUnderstand();
  	  pasteMessage("Good morning!", true);
  	},
  	"good afternoon": function() {
  	  ringUnderstand();
  	  pasteMessage("Good afternoon!", true);
  	},
  	"good night": function() {
  	  ringUnderstand();
  	  pasteMessage("Good night! Have a nice sleep.", true);
  	},
  	"happy birthday": function() {
  	  ringUnderstand();
  	  pasteMessage("I'm glad you remembered. Happy birthday to you too.", true);
  	},
  	"nice haircut": function() {
  	  ringUnderstand();
  	  pasteMessage("I'm glad you noticed. You have a nice haircut too.", true);
  	},
  	"play *song": function(song) {
  	  ringUnderstand();
  	  voicePlaySong(song, true);
  	},
  	"stop playing": function() {
  	  ringUnderstand();
  	  pauseSong();
  	  pasteMessage("Song stopped!", true);
  	},
  	"pause (song)": function() {
  	  ringUnderstand();
  	  pauseSong();
  	  pasteMessage("Song stopped!", true);
  	},
  	"continue playing": function() {
  	  ringUnderstand();
  	  rePlaySong();
  	  pasteMessage("Song resumed!", true);
  	},
  	"(what is the) weather (for today)": function() {
  	  ringUnderstand();
  	  voiceGetWeather(true);
  	},
  	"say that again": function() {
  	  ringUnderstand();
  	  speakText("Sure.");
  	  pasteMessage(outbox.innerText, true);
  	},
  	"set (a) timer for *time minute(s)": function(time) {
  	  ringUnderstand();
  	  if (time <= 0) {
  	  	pasteMessage("Please specify a time greater or equal to 1.", true);
  	  } else {
  	  	if (isNaN(parseFloat(time))) {
  	  	  pasteMessage("I didn't quite catch that number, could you please say the command again?", true);
  	  	} else {
  	  	  if (time > 1) {
  	  	  	pasteMessage("Timer set to ring in " + time + " minutes.", true);
  	  	  	timer = setTimeout(function() {
  	  	  	  ringTimer();
  	  		}, parseInt(time) * 60000);
  	  	  } else {
  	  	  	pasteMessage("Timer set to ring in 1 minute.", true);
  	  	  	timer = setTimeout(function() {
  	  	  	  ringTimer();
  	  		}, 60000);
  	  	  }
  	  	}
  	  }
  	},
  	"stop timer": function() {
  	  ringUnderstand();
  	  clearTimeout(timer);
  	  stopTimer();
  	  pasteMessage("Timer stopped.", true);
  	},
  	"meow": function() {
  	  ringUnderstand();
  	  pasteMessage("Meow.", true);
  	},
  	"be quiet": function() {
  	  ringUnderstand();
  	  pasteMessage("Ok meanie.", true);
  	  annyang.abort();
  	  setTimeout(function() {
  	  	annyang.start();
  	  }, 600000)
  	},
  	"when do I work next": function() {
  	  ringUnderstand();
  	  getWorkShifts("work.txt", function(shiftsArray) {
  	  	pasteMessage("Your next shift is a " + shiftsArray[0][1] + " shift on " + shiftsArray[0][0] + " from " 
  	  	  + shiftsArray[0][2] + ".", true);
  	  });
  	},
  	"when's my next shift": function() {
      ringUnderstand();
  	  getWorkShifts("work.txt", function(shiftsArray) {
		pasteMessage("Your next shift is a " + shiftsArray[0][1] + " shift on " + shiftsArray[0][0] + " from " 
  	  	  + shiftsArray[0][2] + ".", true);
  	  });
  	},
  	"list all (of my) shifts": function() {
  	  ringUnderstand();
  	  getWorkShifts("work.txt", function(shiftsArray) {
  	  	var temp = "Your known work shifts are";
  	 	for (var i = 0; i < shiftsArray.length; i++) {
  	  	  temp = temp + ", " + shiftsArray[i][1] + " shift on " + shiftsArray[i][0] + " from "
  	  	  	+ shiftsArray[i][2];
  	  	}
  	  	pasteMessage(temp, true);
  	  });
  	},
  	"remind me to *thing": function(thing) {
  	  ringUnderstand();
  	  addReminder(thing);
  	  pasteMessage("Reminder to \"" + thing + "\" added.", true);
  	},
  	"list all reminders": function() {
  	  ringUnderstand();
  	  remindYou();
  	  /*if (reminders.length == 0) {
  	  	pasteMessage("You have no reminders.", true);
  	  } else {
  	  	var temp = "Reminders, ";
  	  	for (var i = 0; i < reminders.length; i++) {
  	  	  temp = temp + (i + 1) + ": " + reminders[i] + ". ";
  	  	}
  	  	pasteMessage(temp, true);
  	  }*/
  	},
  	"clear all reminders": function() {
  	  ringUnderstand();
  	  reminders = [];
  	  pasteMessage("Reminders cleared.", true);
  	},
  	"remove reminder *num": function(num) {
  	  ringUnderstand();
  	  reminders.splice(parseInt(num) - 1, 1);
  	  pasteMessage("Reminder " + num + " removed.", true);
  	}
  };

  annyang.addCommands(commands);

  annyang.start();

  annyang.debug(true);

  //console.log("Listening for voice input");

}

function voicePlaySong(song, doVoice) {
  
  findSong(song, function(response, title) {

	console.log(response + " " + title);
	if (response == "blah") {
	  pasteMessage("Couldn't find the song! :(", doVoice);
	} else {
	  pasteMessage(confirm_replies[randomChoose(confirm_replies.length)] + " Now playing: " 
	    + title, doVoice);
	  playSong(response);
	}

  });

}

function voiceGetWeather(doVoice) {
  
  /*getWeatherCurrent("6094817", function(response) {

  var jsondata = JSON.parse(response);

  pasteMessage("Weather for " + jsondata.name + ", " + jsondata.sys.country + " right now is " 
  	+ jsondata.weather[0].description + ". Temperature is " + Math.round(jsondata.main.temp) 
  	+ " degrees C.", true);

  });*/

  // Ottawa
  getWeatherDay("6094817", function(response) {

  	var jsondata = JSON.parse(response);

  	pasteMessage("Weather for " + jsondata.city.name + ", " + jsondata.city.country + " today is " 
  	  + jsondata.list[0].weather[0].description + ". Temperature is " + Math.round(jsondata.list[0].main.temp) 
  	  + " degrees C with a high of " + Math.round(jsondata.list[0].main.temp_max) + " and a low of " 
  	  + Math.round(jsondata.list[0].main.temp_min) + ".", doVoice);

  });
}

// spit out reminders every half hour
var reminderLoop = setInterval(remindYou, 1800000);

// SPEECH SYNTHESIS
var speechEnabled = true;
var speaker;

if (typeof speechSynthesis != "undefined") {
  
  console.log("Browser supports speech synthesis. It will be enabled.");
  
  // set up voice
  var voices = speechSynthesis.getVoices();
  var chosenVoice;

  speakText("Speech has been successfully enabled.");

} else {
  
  console.log("Browser does not support speech synthesis. It will be disabled.");
  speechEnabled = false;

}

function speakText(msg) {

  var speaker = new SpeechSynthesisUtterance();
  var voices = window.speechSynthesis.getVoices();

  speaker.default = false;
  speaker.volume = 1.0;
  speaker.rate = 1.0;
  speaker.pitch = 1.0;
  speaker.voice = voices.filter(function(voice) { return voice.name == 'Google UK English Female'; })[0];
  speaker.lang = 'en-GB';

  speaker.text = msg;

  window.speechSynthesis.speak(speaker);

}