// Thanks to the chap @ https://stackoverflow.com/questions/10473745/compare-strings-javascript-return-of-likely

function similarity(s, t) {
  var longer = s;
  var shorter = t;
  if (s.length < t.length) {
    longer = t;
    shorter = s;
  }
  var longerLength = longer.length;
  if (longerLength == 0) {
    return 1.0;
  }
  return (longerLength - LD(longer, shorter)) / parseFloat(longerLength);
}

function LD(s, t) {
  s = s.toLowerCase();
  t = t.toLowerCase();

  var costs = new Array();
  for (var i = 0; i <= s.length; i++) {
    var lastValue = i;
    for (var j = 0; j <= t.length; j++) {
      if (i == 0)
        costs[j] = j;
      else {
        if (j > 0) {
          var newValue = costs[j - 1];
          if (s.charAt(i - 1) != t.charAt(j - 1))
            newValue = Math.min(Math.min(newValue, lastValue),
              costs[j]) + 1;
          costs[j - 1] = lastValue;
          lastValue = newValue;
        }
      }
    }
    if (i > 0)
      costs[t.length] = lastValue;
  }
  return costs[t.length];
}