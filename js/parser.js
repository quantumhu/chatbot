// Parsing Text Files Code

function loadLines(txtfile) {
  var xhrp;
  if (window.XMLHttpRequest) {
    xhrp = new XMLHttpRequest();
  } else {
    // code for older browsers
    xhrp = new ActiveXObject("Microsoft.XMLHTTP");
  }
  xhrp.onreadystatechange = function() {
    if (xhrp.readyState == 4 && xhrp.status == 200) {
      // split at each new line
      lines = xhrp.responseText.split("\n");

      // get rid of blank line at the end of the last argument of each line
      // then remove that bullshit delimiter at the front
      for (var i = 0; i < lines.length; i++) {
        lines[i] = lines[i].replace(/[\n\r]/g, "");
        lines[i] = lines[i].substring(lines[i].substring(1).search(/[A-Z]/) + 1);
        lines[i] = lines[i].substring(lines[i].search(/[+]\s/) + 2);
      }
    }
  }
  xhrp.open("GET", txtfile, true);
  xhrp.send(null);
}

function loadUserLines(txtfile) {
  var xhrp;
  if (window.XMLHttpRequest) {
    xhrp = new XMLHttpRequest();
  } else {
    // code for older browsers
    xhrp = new ActiveXObject("Microsoft.XMLHTTP");
  }
  xhrp.onreadystatechange = function() {
    if (xhrp.readyState == 4 && xhrp.status == 200) {
      // split at each new line
      userLines = xhrp.responseText.split("\n");

      for (var i = 0; i < userLines.length; i++) {
        userLines[i] = userLines[i].replace(/[\n\r]/g, "");
      }
    }
  }
  xhrp.open("GET", txtfile, true);
  xhrp.send(null);
}