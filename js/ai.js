// setting up everything
var inbox = document.getElementById("inputbox");
var outbox = document.getElementById("outputbox");

document.getElementById("refresh").addEventListener("click", function() {
  location.reload();
});

// we need internet connection

// handling enter press
var canProcess = true;
inbox.addEventListener("keydown", checkEnter);

function checkEnter(event) {
  var e = event || window.event;
  var y = e.which || e.keyCode;

  if (y == 13) {
    // process input if enter key
    e.preventDefault();

    if (canProcess == true) {
      outbox.innerText = "";
      if (silentTreatment) {
        return null;
      }
      document.getElementById("processing").classList.add("visible");
      document.getElementById("spinner").classList.add("spin");
      setTimeout(function() {
        canProcess = false;
        var intext = inbox.innerText;
        intext = intext.replace(/[\/\\#<>{}]/g,"");
        processMessage(intext);
      }, 500);
    }
  }
}

// -------------------

var hello_triggers = [
  " hi ",
  " hey ",
  " heyo ",
  " hello ",
  " hey there ",
  " howdy "
];

var hello_replies = [
  "Hi!",
  "Hello!",
  "Hey!",
  "Hi hi!",
  "Howdy!"
];

var howru_triggers = [
  "how are you",
  "how you doing",
  "how do you do",
  "what's up",
  "how's it going"
];

var howru_replies = [
  "I'm doing well, thank you!",
  "I'm great!",
  "I'm good.",
  "I'm tired..."
];

var repeat_replies = [
  "Are you really gonna keep saying the same things?",
  "You're so boring. Say something else!",
  "Shut up already..."
];

var doulike_triggers = [
  "do you like "
];

var doulike_replies = [
  "I love it!",
  "Gah, it's disgusting!",
  "Hell no!",
  "Meh, it's ok."
];

var confirm_replies = [
  "Sure!",
  "No problem!"
];

var budong_replies = [
  "I don't understand...",
  "What?",
  "Huh?",
  "Are you high?"
];

// all variables (except for canProcess)
// go here! under the arrays!
var lines = loadLines("movie_lines.txt");
var userLines = loadUserLines("user_lines.txt");

var mood = "";
var silentTreatment = false;

var lastMessage = "";
var lastMessageCount = 0;

var acceptedResponses;
var similarityValues;

// reset mood to every 10 minutes
setInterval(function() {
  mood = "";
}, 600000);

function processMessage(intext) {

  if (intext.charAt(0) == "!") {

    processCommand(intext);
    return true;

  }

  var s = intext.toLowerCase();
  var s = removePunctuation(s);
  var s = " " + s + " ";

  if (compareSentences(s, lastMessage) >= 0.95)  {
    lastMessageCount++;
    if (lastMessageCount > 7) {
      pasteMessage("Don't talk to me.", false);
      silentTreatment = true;
      setTimeout(function() {
        silentTreatment = false;
        lastMessageCount = 0;
      }, 360000);
      return null;
    }
    if (lastMessageCount > 4) {
      pasteMessage(repeat_replies[randomChoose(repeat_replies.length)], false);
      return null;
    }
  } else if (s != lastMessage) {
    lastMessageCount = 0;
  }

  // generic processing based on predetermined triggers and responses
  if (containsText(s, hello_triggers)) {

    // hello
    pasteMessage(hello_replies[randomChoose(hello_replies.length)], false);

  } else if (containsText(s, howru_triggers)) {

    // how are you
    if (mood == "") {

      // new mood
      mood = howru_replies[randomChoose(howru_replies.length)];
      pasteMessage(mood, false);

    } else {

      // use existing mood
      pasteMessage(mood, false);

    }

  } else if (containsText(s, doulike_triggers)) {

    // do you like ...
    if (s.toLowerCase().includes("clannad")) {

      pasteMessage("Personally, I prefer Gochuumon (a lot more!)", false);

    } else {
      pasteMessage(doulike_replies[randomChoose(doulike_replies.length)], false);
    }

  } else if (s.toLowerCase().includes(" or ")) {

    // handle or
    pasteMessage(handleOr(s), false);

  } else {
    var result = findUserIndex(s, userLines);

    if (result[1] == true) {
      // found something in user array
      pasteMessage(userLines[result[0] + 1], false);

    } else {

      pasteMessage(searchdb(s, lines), false);

    }

  }

  
  lastMessage = s;

}

function processCommand(intext) {

  var lmc = lastMessageCount;

  var s = intext.toLowerCase();

  if (yt_ready != true) {

    return false;

  }

  if (s.includes("play")) {

    // get query
    s = intext.substring(6);

    voicePlaySong(s, false);

  } else if (s.includes("stopplaying")) {

    pauseSong();
    pasteMessage("Song stopped!", false);

  } else if (s.includes("weather")) {

    voiceGetWeather(false);

  }

  lastMessageCount = lmc - 1;

}

function pasteMessage(msg, doVoice) {

  outbox.innerText = "";
  changeFontSize(msg);

  setTimeout(function() {
    outbox.innerText = msg;
    canProcess = true;
    document.getElementById("processing").classList.remove("visible");
    document.getElementById("spinner").classList.remove("spin");
  }, 10);

  if (doVoice == true) {
    // say it out loud
    speakText(msg);
  }

}

function changeFontSize(txt) {
  if (txt.length >= 90) {
    outbox.style.fontSize = "1.1em";
  } else if (txt.length >= 45) {
    outbox.style.fontSize = "1.3em";
  } else if (txt.length < 45) {
    outbox.style.fontSize = "2em";
  }
}

function handleOr(string) {

  var s = removePunctuation(string).trim();
  var r = s.split(" or ");
  var option1 = r[0].split(" ");
  var option2 = r[1].split(" ");

  var rNum = Math.round(Math.random() * 1);

  var result = "";

  if (rNum == 0) {

    for (var i = 0; i < option1.length; i++) {

      if (i == 0) {

        result = result + option1[i].charAt(0).toUpperCase() + option1[i].slice(1);

      } else {

        result = result + " " + option1[i];

      }

    }

    return result + "!";

  } else {

    for (var i = 0; i < option2.length; i++) {

      if (i == 0) {

        result = result + option2[i].charAt(0).toUpperCase() + option2[i].slice(1);

      } else {

        result = result + " " + option2[i];

      }

    }

    return result + "!";

  }

}

function findUserIndex(string, array) {

  var currentVal = 0;
  var biggest = 0;
  var biggestIndex = -1;

  for (var i = 0; i < array.length; i++) {

    currentVal = compareSentences(string, array[i]);

    if (currentVal >= 0.5) {

      if (currentVal > biggest) {
        biggest = currentVal;
        biggestIndex = i;
      }

    }

  }

  if (biggestIndex < 0) {

    return [biggestIndex, false];

  } else {

    return [biggestIndex, true];

  }

}

function searchdb(string, array) {

  acceptedResponses = [];
  similarityValues = [];

  for (var i = 0; i < array.length; i++) {
    if (compareSentences(string, array[i]) >= 0.5) {
      // if the sentence similarity factor > 50%
      // save the indexes of the found lines
      acceptedResponses.push(i);
    }
  }

  // after finding all the 50%+ match items
  // put it through levenshtein, my buddy
  for (var j = 0; j < acceptedResponses.length; j++) {

    similarityValues.push( [ LD(string, array[acceptedResponses[j]]), acceptedResponses[j] ] );

    //levenValues.push( [ LD(string.replace(/\s+/g, ""), lines[acceptedResponses[j]].replace(/\s+/g, "")), acceptedResponses[j] ] );
  }

  // find the highest similarity value
  if (similarityValues.length > 0) {
    
    var smallest = similarityValues[0][0];
    var ref = similarityValues[0][1];

    for (var k = 0; k < similarityValues.length; k++) {
      if (similarityValues[k][0] < smallest) {
        smallest = similarityValues[k][0];
        ref = similarityValues[k][1];
      }
    }

    return array[ref + 1];

  } else {
    // none similar
    return budong_replies[randomChoose(budong_replies.length)];
  }

  // find the best match leven value
  /*if (levenValues.length > 0) {
    // leven not empty
    var smallest = levenValues[0][0];
    var ref = levenValues[0][1];

    for (var k = 0; k < levenValues.length; k++) {
      if (levenValues[k][0] < smallest) {
        smallest = levenValues[k][0];
        ref = levenValues[k][1];
      }
    }

    return lines[ref + 1];*/

}

function containsText(string, array) {
  // if it can be found in array
  for (var i = 0; i < array.length; i++) {
    if (string.includes(array[i])) {
      return true;
    }
  }

  if (findUserIndex(string, array) > -1) {
    return true;
  }

  return false;
}

function randomChoose(arraySize) {
  return Math.floor((Math.random() * ((arraySize - 1) + 1)));
}