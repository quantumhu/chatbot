## Chatbot

***Disclaimer***: *This code at one point worked but not anymore since a lot of browser features have changed. Hence, some techniques may be outdated and so functionality is not guaranteed.*

This is a basic chatbot (framework?) that I made using JavaScript. I think it's certainly usable in other code like a framework, however, you will have to pull some pieces out as it was unintentionally built like an all-in-one. 

### Features / Implementation:
- **Play Songs from YouTube**
    - The YT API is used to search for songs using the specified keywords. An iframe to an embed version of the video is created on the page that is simply hidden. This embedded video can be freely controlled with the "postMessage" function, generating the play and pause functionality.

- **Get the Weather**
    - Thank you OpenWeatherMap API

- **Understand Voice Commands / Speak**
    - I used a library called "annyang" which is really handy for adding speech recognition anywhere.
    - Anything that is picked up by the speech recognition will be replied to with synthesized speech.

- **Set a Timer and Reminders**
    - Only the functionality for 1 timer at a time was implemented.
    - The reminder feature is rudimentary, as it only can support adding, removing and listing all reminders at once (i.e. no categories, no priority, etc.).
    
- **(Attempt) Normal Conversation**
    - Anything that is typed into the textbox instead of spoken will go through basic text matching to try and create conversation.
        - Basic conversation items like "hi" and "how are you" have pre-set responses and triggers.
        - Anything else that doesn't match a set trigger will go through a huge list of movie script lines. It will attempt to take the input and match it up with a conversation found in these script lines, and will take the following line to *try* to create a conversation. Doesn't work that well sometimes...